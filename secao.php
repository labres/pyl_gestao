<?php
require_once 'modelo/class-email.php';

$dispositivoMovel = verificaDispositivo();

function sec_session_start() {
    error_reporting(0);
    date_default_timezone_set("America/Sao_Paulo");
    $session_name = 'logado'; 
    $secure=false;
    $httponly = true;

    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header('Location: index.php');
        exit();
    }
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"], 
        $cookieParams["domain"], 
        $secure,
        $httponly);
    
    session_name($session_name);
    session_start();
    
    if( $_SERVER['REQUEST_METHOD']=='POST' )
	{  
		$request = md5( implode( $_POST ) );
		
		if( isset( $_SESSION['last_request'] ) && $_SESSION['last_request'] == $request )
		{
            $_SESSION['last_request']  = "";
		}
		else
		{
            $_SESSION['last_request']  = $request;
		}
	}

    session_regenerate_id();    
    
}




function login($email, $senha, $mysqli, $dispositivoMovel) 
{
    $data = date('d/m/Y');
    $hora = date('h:i');
    if ($stmt = $mysqli->prepare("SELECT u.id, nome, email, senha, codigo, t.descricao, data_Inclusao, usuario_inclusao 
                                FROM usuario u INNER JOIN tipo_usuario t ON id_tipo = t.id WHERE email = ? LIMIT 1"))
    { 
        
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($id, $nome, $email, $senha_banco, $codigo, $tipo, $data_inclusao, $usuario_inclusao);
        $stmt->fetch();
        if ($stmt->num_rows == 1) 
        {    
            if ($senha == $senha_banco) 
            { 
                $_SESSION['id'] = $id;
                $_SESSION['nome'] = $nome;
                $_SESSION['email'] = $email;
                $_SESSION['senha'] = $senha_banco;
                $_SESSION['codigo'] = $codigo;
                $_SESSION['tipo'] = $tipo;
                $_SESSION['data_inclusao'] = $data_inclusao;
                $_SESSION['usuario_inclusao'] = $usuario_inclusao;
                return true;
            } 
            else 
            {
                return false;
            }
            
        } else {
            return false;
        }
    }
}

sec_session_start();

function login_check($mysqli) {
    if (isset($_SESSION['email'])) 
    {
        $username = $_SESSION['username'];
        if ($stmt = $mysqli->prepare("SELECT u.id, nome, email, senha, t.descricao, data_Inclusao, usuario_inclusao 
                                    FROM usuario WHERE email = ? LIMIT 1")) 
        {
            $stmt->bind_param('i', $id);
            $stmt->execute();  
            $stmt->store_result();
            if($stmt->num_rows == 1) {
                if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 7200)) 
                {
                    session_unset();     
                    session_destroy();  
                    return false;
                }
                else
                {
                    $_SESSION['LAST_ACTIVITY'] = time();
                     return true;
                }
            }else {
                // Não foi logado 
                return false;
            }
        } 
        else {
            // Não foi logado 
            return false;
        }
    } else {
        // Não foi logado 
        return false;
    }
}
function verificarEmail($emailRecuperacao, $MySQLi){

    $stmt = $MySQLi->prepare("SELECT email, nome FROM usuario WHERE email = ? LIMIT 1");
    $stmt->bind_param('s', $emailRecuperacao);
    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($email, $nome);
    $stmt->fetch();
    $objetoEmail = new Email();
    $objetoEmail->setNome($nome);
    $objetoEmail->setEmail($email);
    if ($stmt->num_rows == 1){
        return $objetoEmail;
    }else{
        return false;
    }
}

function alteraEmail($email, $senha, $MySQLi){
    $stmt = $MySQLi->prepare("UPDATE usuario SET senha = ? WHERE email = ? LIMIT 1");
    $stmt->bind_param('ss', $senha, $email);
    if($stmt->execute() == true){
        session_name('recuperarSenha');
        session_start();
        $_SESSION['emailRecuperacao'] = $email;
        $_SESSION['senhaRecuperacao'] = $senha;
        header('LOCATION: check-login.php');
    }
}

function verificaDispositivo(){
	$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
	$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
	$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
	$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
	$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$symbian = strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");
	$windowsphone = strpos($_SERVER['HTTP_USER_AGENT'],"Windows Phone");

if ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian || $windowsphone == true) {
   return true;
 }else return false;
}
?>
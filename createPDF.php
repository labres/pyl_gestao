<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__.'/vendor/autoload.php';

$dompdf = new Dompdf\Dompdf;

$tabela = file_get_contents('template-orcamento.html');

$dompdf->set_base_path('css/estilo.css');

$dompdf->loadhtml($tabela);

$dompdf->setPaper('A4', 'landscape');



$dompdf->render();
$dompdf->stream();

file_put_contents('cotacao.pdf', $dompdf->output());

?>

<script src="../js/funcoes.js"></script>
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body" id="card-body">

                <h4 class="card-title">Cadastrar orçamento</h4>
                <p class="card-description"> * Obrigatório</p>
                <form class="forms-sample" method="post" id="form" action="#">
    
                    <div class="form-group">
                        <label for="empresa">Empresa</label>
                        <select class="form-control" name="empresa" id="empresa" required>
                            <option value="0" selected>Selecione</option>
                           
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
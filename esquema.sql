create database pyl_gestao;
use pyl_gestao;

create table tipo_usuario(
	id int primary key auto_increment not null,
    descricao varchar(20) not null
); 

create table usuario(
	id int primary key auto_increment not null,
    nome varchar(100) not null,
    email varchar(100) not null,
    senha varchar(10) not null,
    data_inclusao date not null,
    usuario_inclusao varchar(100) not null
    id_tipo int not null,
    constraint fk_tipo FOREIGN KEY (id_tipo)
	REFERENCES tipo_usuario (id)
); 
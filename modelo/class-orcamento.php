<?php
class Orcamento{

    private $id;
    private $cotacao;
    private $descricao;
    private $data;
    private $prazo_entrega;
    private $transporte;
    private $alimentacao;
    private $hospedagem;
    private $validade;
    private $condicao_pagamento;
    private $solicitante;
    private $comprador;
    private $id_cliente;
    private $owner;
    private $tipo_registro;

    //add all field type string in array
    const ARRAY_FIELDS_STRING =  array('cotacao', 'descricao', 'data', 'tipo_registro' );

    public function _construct(){}

    public function getID(){
        return $this->id;
    }
    public function setId( $id ){
        $this->id = $id;
    }

    public function getCotacao(){
        return $this->cotacao;
    }
    public function setCotacao( $cotacao ){
        $this->cotacao = $cotacao;
    }
    
    public function getDescricao(){
        return $this->descricao;
    }
    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }

    public function getData(){
        return $this->data;
    }
    public function setData( $data ){
        $this->data = $data;
    }

    public function getPrazo(){
        return $this->prazo_entrega;
    }
    public function setPrazo( $prazo_entrega ){
        $this->prazo_entrega = $prazo_entrega;
    }

    public function getTransporte(){
        return $this->transporte;
    }
    public function setTransporte( $transporte ){
        $this->transporte = $transporte;
    }

    public function getAlimentacao(){
        return $this->alimentacao;
    }
    public function setAlimentacao( $alimentacao ){
        $this->alimentacao = $alimentacao;
    }

    public function getHospedagem(){
        return $this->hospedagem;
    }
    public function setHospedagem( $hospedagem ){
        $this->hospedagem = $hospedagem;
    }

    public function getValidade(){
        return $this->validade;
    }
    public function setValidade( $validade ){
        $this->validade = $validade;
    }

    public function getCondicao(){
        return $this->condicao_pagamento;
    }
    public function setCondicao( $condicao_pagamento ){
        $this->condicao_pagamento = $condicao_pagamento;
    }

    public function getIdSolicitante(){
        return $this->solicitante;
    }
    public function setIdSolicitante( $solicitante ){
        $this->solicitante = $solicitante;
    }

    public function getIdComprador(){
        return $this->comprador;
    }
    public function setIdComprador( $comprador ){
        $this->comprador = $comprador;
    }

    public function getIdCliente(){
        return $this->id_cliente;
    }
    public function setIdCliente( $id_cliente ){
        $this->id_cliente = $id_cliente;
    }

    public function getOwner(){
        return $this->owner;
    }
    public function setOwner( $owner ){
        $this->owner = $owner;
    }

    public function getTipoOrcamento(){
        return $this->tipo_registro;
    }
    public function setTipoOrcamento( $tipo_registro ){
        $this->tipo_registro = $tipo_registro;
    }



    public function getAllOrcamentos( $MySQLi ){
        try{
            $sql = 'SELECT * FROM orcamento';
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return $e->getMessage(); 
        }
    }

    public function getLastOrcamento( $MySQLi ){
        try{
            $sql = 'SELECT * FROM orcamento ORDER BY id DESC LIMIT 1';
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return $e->getMessage();
        }
    }

    public function save( $MySQLi ){

        try{
            $sql = 'INSERT INTO orcamento' . $this->buildValuesToInsert();
            $MySQLi->query( $sql );
            return true;

        }catch( Exception $e ){
            return $e->getMessage();
        }
    }

    public function buildValuesToInsert(){
       
        $sqlValues = ') VALUES(';
        $sqlFields = '(';

        foreach( $this as $key=>$value ){
             
            if (in_array($key, $this::ARRAY_FIELDS_STRING )){
                if ( $value != '' ){
                    $sqlValues .=  '"'.$value.'", ';
                    $sqlFields .= $key . ', ';
                } 
                continue;
            }   

            if ($value != ''){
                $sqlValues .= $value.', ';
                $sqlFields .= $key . ', ';
            } 
        }

        return substr($sqlFields,0,-2) . substr($sqlValues,0,-2 ).')';
    }
}
?>
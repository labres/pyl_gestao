<?php

class Item{

    private $id;
    private $descricao;

    public function Item(){}
    
    public function __construct(){}

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setDescricao($descricao){
        $this->descricao = $descricao;
    }

    public function getDescricao(){
        return $this->descricao;
    }



    public function salvarItem( $MySQLi ){
        try{
            $sql = "INSERT INTO item (descricao) VALUES ('".$this->getDescricao()."')";
            $MySQLi->query($sql);
            return true;
        }catch(Exception $e){
            return false;
        }
    }

    public function buscarItem( $MySQLi ){
        try{
            $sql = 'SELECT id, descricao FROM item ORDER BY descricao ASC';
            $resultado = $MySQLi->query($sql);
            return $resultado;
        }catch(Exception $e){
            return '';
        }
    }

    public function removeItemById( $MySQLi, $id ){

        try{
            if ( $id == null || $id == '' ) return false;
            $sql = 'DELETE FROM item WHERE id = '. $id;
            $resultado = $MySQLi->query($sql);
            return true;
        
        }catch(Exception $e){
            return false;
        }
    }

}

?>
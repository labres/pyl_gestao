<?php
class configuracao{

    private $id;
    private $descricao;
    private $id_tipo;
    private $value;

    public function _construct(){}

    public function getId(){
        return $this->id;
    }
    public function setId( $id ){
        $this->id = $id;
    }

    public function getDescricao(){
        return $this->descricao;
    }
    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }

    public function getIdTipo(){
        return $this->id_tipo;
    }
    public function setIdTipo( $id_tipo ){
        $this->id_tipo = $id_tipo;
    }

    public function getValue(){
        return $this->value;
    } 
    public function setValue( $value ){
        $this->value = $value;
    }

    public function findAllConfiguration( $MySQLi ){
        try{
            $sql = 'SELECT *FROM configuracao';
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return false;
        }
    }

    public function findConfigurationById( $MySQLi, $id ){
        try{
            $sql = 'SELECT * FROM configuracao WHERE id = '. $id;
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return false;
        }
    }

    public function addConfiguration( $MySQLi ){
        try{
            $sql = 'INSERT INTO configuracao (descricao, id_tipo) VALUES ( "'. $this->getDescricao() .'" , '. $this->getIdTipo() .')';
            $MySQLi->query( $sql );
            return true;
        }catch( Exception $e ){
            return false;
        }
    }
}
?>
<?php  
class Usuario{
        private $id;
        private $nome;
        private $email;
        private $senha;
        private $id_tipo;
        private $id_cliente;
        private $data_inclusao;
        private $usuario_inclusao;
        private $descricao;

        public function Usuario(){}

        public function __construct(){}
            
        public function setId($id){
                $this->id = $id;
        }
        public function getId(){
                return $this->id;
        }
        public function setNome($nome){
            $this->nome = $nome;
        }
        public function getNome(){
                return $this->nome;
        }
        public function setEmail($email){
            $this->email = $email;
        }
        public function getEmail(){
            return $this->email;
        }
        public function setTelefone($telefone){
            $this->telefone = $telefone;
        }
        public function getTelefone(){
            return $this->telefone;
        }
        public function setCelular($celular){
            $this->celular = $celular;
        }
        public function getCelular(){
            return $this->celular;
        }
        public function setSenha($senha){
            $this->senha = $senha;
        }
        public function getSenha(){
            return $this->senha;
        }
        public function setIdTipo($id_tipo){
            $this->id_tipo = $id_tipo;
        }
        public function getIdTipo(){
            return $this->id_tipo;
        }
        public function setIdCliente($id_cliente){
            $this->id_cliente = $id_cliente;
        }
        public function getIdCliente(){
            return $this->id_cliente;
        }
        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }
        public function getDescricao(){
            return $this->descricao;
        }
        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }
        public function getCodigo(){
            return $this->codigo;
        }
        public function setIdCargo($cargo){
            $this->cargo = $cargo;
        }
        public function getIdCargo(){
            return $this->cargo;
        }
        public function setDataInclusao($dataInclusao){
            $this->data_inclusao = $dataInclusao;
        }
        public function getDataInclusao(){
            return $this->data_inclusao;
        }
        public function setUsuarioInclusao($usuarioInclusao){
            $this->usuario_inclusao = $usuarioInclusao;
        }
        public function getUsuarioInclusao(){
            return $this->usuario_inclusao;
        }
        public function salvarUsuario($MySQLi){
            try{
                $sql = "INSERT INTO usuario (nome, email, telefone, celular, senha, id_tipo,id_cliente, codigo, id_cargo, data_inclusao, usuario_inclusao) 
                        VALUES ('".$this->getNome()."', '".$this->getEmail()."','".$this->getTelefone()."','".$this->getCelular()."', '".$this->getSenha()."', 
                        ".$this->getIdTipo().",".$this->getIdCliente().",'".$this->getCodigo()."',".$this->getIdCargo().",'".$this->getDataInclusao()."', 
                        '".$this->getUsuarioInclusao()."')";
                $MySQLi->query($sql);
                return true;
            }catch(Exception $e){
                return false;
            }
        }
        public function buscarUsuarios($MySQLi){
            try{
                $sql = 'SELECT u.id, u.nome, u.email, u.senha, t.descricao as descricao, u.id_cargo,  u.id_cliente, u.data_inclusao, u.usuario_inclusao
                        FROM usuario u  INNER JOIN tipo_usuario t ON u.id_tipo = t.id ORDER BY u.nome ASC'; 
                $resultado = $MySQLi->query($sql);
                return $resultado;
            }catch(Exception $e){
                return false;
            }
        }

        public function excluirUsuario($id, $MySQLi){
            try{
                $sql = 'DELETE FROM usuario WHERE id = '.$id; 
                $MySQLi->query($sql);
                return true;
            }catch(Exception $e){
                return false;
            }
        }

        public function buscarUsuario($id, $MySQLi){
            try{
                $sql = 'SELECT * FROM usuario WHERE id = '.$id; 
                $resultado = $MySQLi->query($sql);
                return $resultado;
            }catch(Exception $e){
                return false;
            }
        }

        public function findUserByCompany($MySQLi, $idCompany ){
            try{
                $sql = 'SELECT id, nome 
                        FROM usuario 
                        WHERE id_cliente = '. $idCompany .' 
                        ORDER BY nome ASC';

                return $MySQLi->query($sql);
            }catch(Exception $e){
                return false;
            }
        }

        public function findBuyerByCompany( $MySQLi, $idCompany ){
            try{
                $sql = 'SELECT id, nome 
                        FROM usuario
                        WHERE id_cliente = ' . $idCompany .' AND id_tipo = 3
                        ORDER BY nome ASC';
                return $MySQLi->query( $sql );
                
            }catch( Exception $e ){
                return $e->getMessage();
            }
        }

        function editarUsuario($usuario, $MySQLi){
            try{
                $sql = 'UPDATE usuario SET nome = "'.$usuario['nome'].'",  email = "'.$usuario['email'].'"
                        , telefone = "'.$usuario['telefone'].'",celular = "'.$usuario['celular'].'", senha = "'.$usuario['senha'].'"
                        , id_cargo = '.$usuario['cargo'].', codigo = "'.$usuario['codigo'].'", id_tipo = '.$usuario['tipoUsuario'].',id_cliente = '.$usuario['cliente'].', 
                         data_inclusao = "'.$usuario[0]['dataInclusao'].'", usuario_inclusao = "'.$usuario[1]['usuarioInclusao'].'" 
                         WHERE id = '. $usuario['idUsuario'];
                $MySQLi->query($sql);
                return true;
            }catch(Exception $e){
                return false;
            }
        }

        function editarSenha($post, $MySQLi){
            try{
                $sql = 'UPDATE usuario SET senha = "'.$post['senha2'].'" WHERE id = '. $post['idUsuario'];
                $MySQLi->query($sql);
                return true;
            }catch(Exception $e){
                return false;
            }
        }
    }

?>
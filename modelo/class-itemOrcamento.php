<?php
class ItemOrcamento{

    public function _contruct(){}
        
    private $id;
    private $id_item;
    private $id_orcamento;
    private $valor;
    private $tempo;

    public function getId(){
        return $this->id;
    }
    public function setId( $id ){
        $this->id = $id;
    }

    public function getIdItem(){
        return $this->id_item;
    }
    public function setIdItem( $id_item ){
        $this->id_item = $id_item;
    }

    public function getIdOrcamento(){
        return $this->id_orcamento;
    }
    public function setIdOrcamento( $id_orcamento ){
        $this->id_orcamento = $id_orcamento;
    }

    public function getValor(){
        return $this->valor;
    }
    public function setValor( $valor ){
        $this->valor = $valor;
    }

    public function getTempo(){
        return $this->tempo;
    }
    public function setTempo( $tempo ){
        $this->tempo = $tempo;
    }

    public function getAllItemOrcamentos( $MySQLi ){
        try{
            $sql = 'SELECT * FROM itemOrcamento';
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return $e->getMessage();
        }
    }

    public function getItemOrcamentosByIdOrcamento( $MySQLi, $idOrcamento ){
        try{
            $sql = 'SELECT * FROM itemOrcamento WHERE id = '. $idOrcamento;
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return $e->getMessage();
        }
    }

    public function save( $MySQLi ){
        try{
            $sql = 'INSERT INTO itemOrcamento '. $this->buildValuesToInsert() ;  
            $MySQLi->query( $sql );
            return true;
            
        }catch( Exception $e ){
            return $e->getMessage();
        }
    }

    public function buildValuesToInsert(){
       
        $sqlValues = ') VALUES(';
        $sqlFields = '(';

        foreach( $this as $key=>$value ){

            if ( $value != '' ){
                $sqlValues .=  $value.', ';
                $sqlFields .= $key . ', ';
            } 
        }

        return substr($sqlFields,0,-2) . substr($sqlValues,0,-2 ).')';
    }
}
?>
<?php  
class Cliente{
    private $id;
    private $nome;
    private $cnpj;
    private $telefone;
    private $data_inclusao;
    private $usuario_inclusao;
    private $codigo;

    public function Cliente(){}

    public function __construct(){}
                
    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function setNome($nome){
        $this->nome = $nome;
    }
    public function getNome(){
        return $this->nome;
    }
    public function setCnpj($cnpj){
        $this->cnpj = $cnpj;
    }
    public function getCnpj(){
        return $this->cnpj;
    }
    public function setTelefone($telefone){
        $this->telefone = $telefone;
    }
    public function getTelefone(){
        return $this->telefone;
    }
    public function setDataInclusao($data_inclusao){
        $this->data_inclusao = $data_inclusao;
    }
    public function getDataInclusao(){
        return $this->data_inclusao;
    }
    public function setUsuarioInclusao($usuario_inclusao){
        $this->usuario_inclusao = $usuario_inclusao;
    }
    public function getUsuarioInclusao(){
        return $this->usuario_inclusao;
    }

    public function getCodigo(){
        return $this->codigo;
    }
    public function setCodigo( $codigo ){
        $this->codigo = $codigo;
    }

    public function salvarCliente($MySQLi){
        try{
            $sql = 'INSERT INTO cliente (nome, telefone, cnpj, data_inclusao, usuario_inclusao, codigo) 
                    VALUES ("'.$this->getNome().'","'.$this->getTelefone().'","'.$this->getCnpj().'","'.$this->getDataInclusao().'", 
                    "'.$this->getUsuarioInclusao().'", "'.$this->getCodigo().'")';
            $MySQLi->query($sql);
            return true;
        }catch(Exception $e){
            return false;
        } 
    }
    public function buscarClientes($MySQLi){
        try{
            $sql = 'SELECT id, nome, telefone, cnpj, data_inclusao, usuario_inclusao, codigo
                    FROM cliente ORDER BY nome ASC'; 
            $resultado = $MySQLi->query($sql);
            return $resultado;
        }catch(Exception $e){
            return false;
        }
    }

    public function excluirCliente($id, $MySQLi){
        try{
            $sql = 'DELETE FROM cliente WHERE id = '.$id; 
            $MySQLi->query($sql);
            return true;
        }catch(Exception $e){
            return false;
        }
    }

    public function buscarCliente($id, $MySQLi){
        try{
            $sql = 'SELECT * FROM cliente WHERE id = '.$id; 
            $resultado = $MySQLi->query($sql);
            return $resultado;
        }catch(Exception $e){
            return false;
        }
    }

    function editarCliente($cliente, $MySQLi){
        try{
            $sql = 'UPDATE cliente SET nome = "'.$cliente['nome'].'", telefone = "'.$cliente['telefone'].'", cnpj = "'.$cliente['cnpj'].'",  
                     data_inclusao = "'.$cliente[0]['dataInclusao'].'", usuario_inclusao = "'.$cliente[1]['usuarioInclusao'].'" 
                     WHERE id = '. $cliente['idCliente'];
            $MySQLi->query($sql);
            return true;
        }catch(Exception $e){
            return false;
        }
    }
}
?>
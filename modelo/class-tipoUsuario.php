<?php
 
    class TipoUsuario{
        
        private $id;
        private $descricao;

        public function TipoUsuario(){}

        public function __construct(){
            
        }
        public function setId($id){
            $this->id = $id;
        }
        public function getId(){
            return $this->id;
        }
        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }
        public function getDescricao(){
            return $this->descricao;
        }

        public function buscarTiposUsuario($MySQLi){ 
            try{
                $sql = 'SELECT * FROM tipo_usuario ORDER BY descricao ASC';
                $resultado = $MySQLi->query($sql);
                return $resultado;
            }catch(Exception $e){
                return false;
            }
        }
    }
    

?>
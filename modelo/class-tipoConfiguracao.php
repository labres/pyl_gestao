<?php
class TipoConfiguracao{

    private $id;
    private $descricao;
    public function _construct(){}

    public function getId(){
        return $this->id;
    }
    public function setId( $id ){
        $this->id = $id;
    }

    public function getDescricao(){
        return $this->descricao;
    }
    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }

    public function findAllTipoDescricao( $MySQLi ){
        try{
            $sql = 'SELECT * FROM tipo_configuracao';
            return $MySQLi->query( $sql );
        }catch( Exception $e ){
            return false;
        }
    }
} 

?>
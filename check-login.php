<?php
require_once 'conn.php';
require_once 'secao.php';
require_once 'email-senha.php';

$dispositivoMovel = verificaDispositivo();

$email =  $_POST['email'];
$senha =  $_POST['senha'];


if(isset($_SESSION['emailRecuperacao']) && $_SESSION['emailRecuperacao'] != null){
	$email =  $_SESSION['emailRecuperacao'];
	$senha =  $_SESSION['senhaRecuperacao'];
}

if(isset($_POST['emailRecuperacao']) && $_POST['emailRecuperacao'] != null){
	$emailRecuperacao = $_POST['emailRecuperacao'];
	$emailRetorno = verificarEmail($emailRecuperacao, $MySQLi);
	if($emailRetorno == false){
		header('LOCATION:direcionador.php?erro');
	}else{
		enviarEmail($MySQLi, $emailRetorno, 'emailRecuperacao');
	}
}
if($email != null){
	if (login($email, $senha, $MySQLi, $dispositivoMovel) == true)
	{        			   
		echo "<script>window.location='pages/'</script>";	
	}
	else	
	{
		header('LOCATION:index.php?erroAutenticacao');
	} 
}	 
?>
<?php
require_once 'conn.php';
require_once 'secao.php';
require_once 'modelo/class-item.php';

error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set("log_errors", 1);
ini_set("error_log", "/tmp/erro.log");

    if ( isset($_GET['idRemoveItem']) && $_GET['idRemoveItem'] != null ){

        $item = new Item();
        $item->removeItemById( $MySQLi, $_GET['idRemoveItem'] );
    }

    if ( isset( $_POST['item-name'] ) && $_POST['item-name'] != null ){
        $item = new Item();
        $item->setDescricao($_POST['item-name']);
        $item->salvarItem($MySQLi);

        $itens = $item->buscarItem( $MySQLi );
        $itemArray = array();
        $count = 0;
        while ($dados = $itens->fetch_assoc()){ 
            $item = new Item();
            $item->setId($dados['id']);
            $item->setDescricao($dados['descricao']);
            $itemArray[$count] = json_encode($item);
            $count++;
        }

        echo json_encode($itemArray);
    }


?>
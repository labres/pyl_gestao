<?php
require_once 'conn.php';
require_once 'secao.php';

 if(isset($_POST['email']) && $_POST['email'] != null){
    alteraEmail($_POST['email'], $_POST['senha2'], $MySQLi);
 }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PYL gestão</title>
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="images/logo-pyl.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="text-center brand-logo">
                  <img src="images/logo-pyl-login.png">
                </div>
                <h6 class="text-center">Crie uma senha com mínimo de 6 e máximo de 10 caracteres.</h6>
                <div style="display:none" id="mensagemErro" class="text-center">
                  <h4 id="msgErro" class="card-title">Senhas não conferem.</h4>
                </div>
                <form class="pt-3" action="#" method="post">
                  
                <input type="hidden" name="email" class="form-control form-control-lg" value="<?=$_GET['email'] ?>">
                  <div class="form-group">
                    <input type="password" name="senha1" onkeyUp="comparaSenhas()" class="form-control form-control-lg" id="senha1" placeholder="Senha" minlength="6" maxlength="10">
                  </div>
                  <div class="form-group">
                    <input type="password" name="senha2" onkeyUp="comparaSenhas()" class="form-control form-control-lg" id="senha2" placeholder="Repita a senha" minlength="6" maxlength="10">
                  </div>
                  <div class="mt-3">
                    <button id="btn-alterar" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit" disabled="disabled">Salvar Senha</button>
                  </div> 
                </form>
                <a href="index.php"><button class="btn btn-block btn-link btn-lg font-weight-medium auth-form-btn" type="submit">Cancelar</button></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <script src="js/off-canvas.js"></script>
    <script src="js/misc.js"></script>
  </body>
</html>


<script>
  function comparaSenhas(){
    senha1 = document.getElementById('senha1').value;
    senha2 = document.getElementById('senha2').value;
    if(senha2 != ""){
      if(senha1 === senha2){ 
        document.getElementById('msgErro').innerHTML = 'Senhas conferem :)';
        document.getElementById('msgErro').style.color = 'green';
        document.getElementById('btn-alterar').disabled = false;
      }else{
        document.getElementById('mensagemErro').style.display = 'block';
        document.getElementById('msgErro').innerHTML =  'Senhas não conferem :(';
        document.getElementById('msgErro').style.color = 'red';
        document.getElementById('btn-alterar').disabled = true;
      }
    }
  }
</script>
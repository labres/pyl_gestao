<?php
require_once '../modelo/class-tipoUsuario.php';

    /*funções tipos de usuarios*/ 
    function buscarTiposUsuarios($MySQLi){
        $tipo_usuario = new TipoUsuario();
        $tipos_usuarios = $tipo_usuario->buscarTiposUsuario($MySQLi);
        $tipoUsuarioArray = array();
        $count = 0;
        while ($dados = $tipos_usuarios->fetch_assoc()){ 
            $tipo_usuario = new TipoUsuario();
            $tipo_usuario->setId($dados['id']);
            $tipo_usuario->setDescricao($dados['descricao']);
            $tipoUsuarioArray[$count] = $tipo_usuario;
            $count++;
        }
        return $tipoUsuarioArray;
    }
?>
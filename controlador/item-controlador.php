<?php
require_once '../modelo/class-item.php';

function salvarItem($post, $MySQLi){
    $item = new Item();
    $item->setDescricao($post['item']);
    $item->salvarItem($MySQLi);
}

function buscarItens( $MySQLi ){
    $item = new Item();
    $itens = $item->buscarItem($MySQLi);
    $itemArray = array();
    $count = 0;
    while ($dados = $itens->fetch_assoc()){ 
        $item = new Item();
        $item->setId($dados['id']);
        $item->setDescricao($dados['descricao']);
        $itemArray[$count] = $item;
        $count++;
    }
    return $itemArray;
}

function removeItemById( $MySQLi, $id ){
    $item = new Item();
    return $item->removeItemById( $MySQLi, $id );
}

?>
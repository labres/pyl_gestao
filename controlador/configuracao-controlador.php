<?php
require_once '../modelo/class-configuracao.php';

function findConfigurationById( $MySQLi, $id ){
    $configuracao = new Configuracao();
    $result = $configuracao->findAllConfiguration( $MySQLi );
    while( $dados = $result->fetch_assoc() ){
        $configuracao = new Configuracao();
        $configuracao->setId( $dados['id'] );
        $configuracao->setDescricao( $dados['descricao'] );
        $configuracao->setValue( $dados['value'] );
    }
    return $configuracao;
}
?>
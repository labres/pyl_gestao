<?php
require_once '../secao.php';
require_once '../modelo/class-cliente.php';
require_once '../conn.php';

if(isset($_POST['idExcluir']) && $_POST['idExcluir'] != null){
    $id = $_POST['idExcluir'];
    $cliente = new Cliente();
    if($cliente->excluirCliente($id, $MySQLi) == true){
        echo "<script>window.location='../pages/index.php?conteudo=lista-cliente.php&notificacao=excluido'</script>";
    }
    else{
        echo "<script>window.location='../pages/index.php?conteudo=lista-cliente.php&notificacao=erro'</script>";
    }
}

function salvarCliente($post, $MySQLi){
    date_default_timezone_set("America/Sao_Paulo");
    $dataInclusao = date("Y-m-d");
    $cliente = new Cliente();
    $cliente->getCodigo($post['codigo']);
    $cliente->setNome($post['nome']);
    $cliente->setTelefone($post['telefone']);
    $cliente->setCnpj($post['cnpj']);
    $cliente->setDataInclusao($dataInclusao);
    $cliente->setUsuarioInclusao($_SESSION['nome']);
    if($cliente->salvarCliente($MySQLi)== true){
        echo "<script>window.location='../pages/index.php?conteudo=lista-cliente.php&notificacao=salvo'</script>";
    }
    else{
        echo "<script>window.location='../pages/index.php?conteudo=lista-cliente.php&notificacao=erro'</script>";
    }
}

 function buscarClientes($MySQLi){
    $cliente = new Cliente();
    $clientes = $cliente->buscarClientes($MySQLi);
    $array = array();
    $count = 0;
    while ($dados = $clientes->fetch_assoc()){ 
        $cliente = new Cliente();
        $cliente->setId($dados['id']);
        $cliente->setCodigo($dados['codigo']);
        $cliente->setNome($dados['nome']);
        $cliente->setTelefone($dados['telefone']);
        $cliente->setCnpj($dados['cnpj']);
        $array[$count] = $cliente;
        $count++;
    }
    return $array;
}

function buscarCliente($id, $MySQLi){
    $cliente = new Cliente();
    $retorno = $cliente->buscarCliente($id, $MySQLi);
    while ($dados = $retorno->fetch_assoc()){ 
        $cliente->setId($dados['id']);
        $cliente->setCodigo($dados['codigo']);
        $cliente->setNome($dados['nome']);
        $cliente->setTelefone($dados['telefone']);
        $cliente->setCnpj($dados['cnpj']);
        $cliente->setCodigo( $dados['codigo'] );
    }
    return $cliente;
}



function editarCliente($post, $MySQLi){
    date_default_timezone_set("America/Sao_Paulo");
    $dataInclusao = date("Y-m-d"); 
    array_push($post, ["dataInclusao"=>date("Y-m-d")], ["usuarioInclusao"=>$_SESSION['nome']]);
    $cliente = new Cliente();
    if($cliente->editarCliente($post, $MySQLi) == true){
        echo "<script>window.location='../pages/index.php?conteudo=lista-cliente.php&notificacao=editado'</script>";
    }
    else{
        echo "<script>window.location='../pages/index.php?conteudo=lista-cliente.php&notificacao=erro'</script>";
    }
}
?>
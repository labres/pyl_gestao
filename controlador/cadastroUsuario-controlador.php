<?php
require_once '../secao.php';
require_once '../modelo/class-usuario.php';
require_once '../conn.php';


if(isset($_POST['idExcluir']) && $_POST['idExcluir'] != null){
    $id = $_POST['idExcluir'];
    $usuario = new Usuario();
    if($usuario->excluirUsuario($id, $MySQLi) == true){
        echo "<script>window.location='../pages/index.php?conteudo=lista-usuario.php&notificacao=excluido'</script>";
        }
    else{
        echo "<script>window.location='../pages/index.php?conteudo=lista-usuario.php&notificacao=erro'</script>";
    }
}


function salvarUsuario($post, $MySQLi){
    date_default_timezone_set("America/Sao_Paulo");
    $dataInclusao = date("Y-m-d");
    $usuario = new Usuario();
    $usuario->setNome($post['nome']);
    $usuario->setEmail($post['email']);
    $usuario->setTelefone($post['fone']);
    $usuario->setCelular($post['celular']);
    $usuario->setSenha($post['senha']);
    $usuario->setIdTipo($post['tipoUsuario']);
    $usuario->setIdCliente($post['cliente']);
    $usuario->setCodigo($post['codigo']);
    $usuario->setIdCargo($post['cargo']);
    $usuario->setDataInclusao($dataInclusao);
    $usuario->setUsuarioInclusao($_SESSION['nome']);

    if($usuario->salvarUsuario($MySQLi)== true){
        echo "<script>window.location='../pages/index.php?conteudo=lista-usuario.php&notificacao=salvo'</script>";
    }
    else{
        echo "<script>window.location='../pages/index.php?conteudo=lista-usuario.php&notificacao=erro'</script>";
    }
}

function buscarUsuarios($MySQLi){
    $usuario = new Usuario();
    $usuarios = $usuario->buscarUsuarios($MySQLi);
    $array = array();
    $count = 0;
    while ($dados = $usuarios->fetch_assoc()){ 
        $usuario = new Usuario();
        $usuario->setId($dados['id']);
        $usuario->setNome($dados['nome']);
        $usuario->setEmail($dados['email']);
        $usuario->setIdTipo($dados['id_tipo']);
        $usuario->setIdCargo($dados['id_cargo']);
        $usuario->setIdCliente($dados['id_cliente']);
        $usuario->setDescricao($dados['descricao']);
        $array[$count] = $usuario;
        $count++;
    }
    return $array;
}

function buscarUsuario($id, $MySQLi){
    $usuario = new Usuario();
    $retorno = $usuario->buscarUsuario($id, $MySQLi);
    while ($dados = $retorno->fetch_assoc()){ 
        $usuario->setId($dados['id']);
        $usuario->setNome($dados['nome']);
        $usuario->setEmail($dados['email']);
        $usuario->setTelefone($dados['telefone']);
        $usuario->setCelular($dados['celular']);
        $usuario->setIdCargo($dados['id_cargo']);
        $usuario->setIdTipo($dados['id_tipo']);
        $usuario->setIdCliente($dados['id_cliente']);
        $usuario->setDescricao($dados['descricao']);
        $usuario->setSenha($dados['senha']);
        $usuario->setCodigo($dados['codigo']);
    }
    return $usuario;
}

function findUserByCompany( $MySQLi, $idCompany ){
    $usuario = new Usuario();
    $usuarios = array();
    $result = $usuario->findUserByCompany( $MySQLi, $idCompany );
    if ( $result == null ) return $result;
    while( $data =  $result->fetch_assoc() ){
        $usuario = new Usuario();
        $usuario->setId($data['id']);
        $usuario->setNome($data['nome']);
        array_push($usuarios, $usuario);
    }
    return $usuarios;
}

function findBuyerByCompany( $MySQLi, $idCompany ){
    $buyer = new Usuario();
    $buyers = array();
    $result = $buyer->findBuyerByCompany( $MySQLi, $idCompany );
    if ( $result == null ) return $result;
    while( $data =  $result->fetch_assoc() ){
        $buyer = new Usuario();
        $buyer->setId($data['id']);
        $buyer->setNome($data['nome']);
        array_push($buyers, $buyer);
    }
    return $buyers;
}

function editarUsuario($post, $MySQLi){
    date_default_timezone_set("America/Sao_Paulo");
    $dataInclusao = date("Y-m-d"); 
    array_push($post, ["dataInclusao"=>date("Y-m-d")], ["usuarioInclusao"=>$_SESSION['nome']]);
    $usuario = new Usuario();
    $usuario->editarUsuario($post, $MySQLi);
    if($usuario->editarUsuario($post, $MySQLi) == true){
        echo "<script>window.location='../pages/index.php?conteudo=lista-usuario.php&notificacao=editado'</script>";
    }
    else{
        echo "<script>window.location='../pages/index.php?conteudo=lista-usuario.php&notificacao=erro'</script>";
    }
}

function editarSenha($post, $MySQLi){
    $usuario = new Usuario();
    if($usuario->editarSenha($post, $MySQLi) == true){
        return true;
    }else{
        return false;
    }
}
?>
<?php
require_once '../modelo/class-cargo.php';

function salvarCargo($post, $MySQLi){
    $cargo = new Cargo();
    $cargo->setDescricao($post['cargo']);
    $cargo->salvarCargo($MySQLi);
}

function buscarCargo($MySQLi){
    $cargo = new Cargo();
    $cargos = $cargo->buscarCargo($MySQLi);
    $cargoArray = array();
    $count = 0;
    while ($dados = $cargos->fetch_assoc()){ 
        $cargo = new Cargo();
        $cargo->setId($dados['id']);
        $cargo->setDescricao($dados['descricao']);
        $cargoArray[$count] = $cargo;
        $count++;
    }
    return $cargoArray;
}
?>
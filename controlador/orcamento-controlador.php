<?php
    require_once '../secao.php';
    require_once '../modelo/class-orcamento.php';
    require_once '../modelo/class-itemOrcamento.php';
    // require_once '../template-orcamento.php';
    //require_once '../createPDF.php';

    function save( $MySQLi, $post) {
        
        $orcamento = new Orcamento();
        $orcamento->setCotacao( $post['codigo'] );
        $orcamento->setDescricao( $post['descriao'] );
        $orcamento->setData( date("yy-m-d") );
        $orcamento->setPrazo( $post['prazo'] );
        $orcamento->setTransporte( parseCurrencyToFloat($post['transporte']));
        $orcamento->setAlimentacao( parseCurrencyToFloat($post['alimentacao']));
        $orcamento->setHospedagem( parseCurrencyToFloat($post['hospedagem']));
        $orcamento->setValidade( parseCurrencyToFloat($post['validade'] ));
        $orcamento->setCondicao( $post['condicao'] );
        $orcamento->setIdSolicitante( $post['solicitante'] );
        $orcamento->setIdComprador( $post['comprador'] );
        $orcamento->setIdCliente( $post['empresa'] );
        $orcamento->setOwner( $post['idUsuario'] );
        $orcamento->setTipoOrcamento( $post['typeSave'] );
        
        //$orcamento->save( $MySQLi );

        $orcamento = $orcamento->getLastOrcamento( $MySQLi );
        $idOrcamento = $orcamento->fetch_assoc()['id'];

        for( $i = 0; $i < count($post['item']); $i++ ){

            $item = new ItemOrcamento();
            $item->setIdItem( intval($post['item'][$i]));
            $item->setIdOrcamento( $idOrcamento );
            $item->setValor( parseCurrencyToFloat($post['valor'][$i]));
            $item->setTempo(intval($post['tempo'][$i]));
            //$item->save( $MySQLi );
        }
        echo 'testando';
        header('Location: ../pages/index.php?conteudo=createPDF.php');

        
        //echo "<script>window.location='../pages/index.php?succes'</script>";
    }

    function parseCurrencyToFloat( $value ){
        $value = str_replace( 'R$', '', $value );
        $value = str_replace( ',', '.', $value );
        return $value;
    }

    function buildCodigoOrcamento( $codigoCliente ){
        return $_SESSION['codigo'] .'-'. $codigoCliente .'-'.date("Y").'-0005';
    }
?>
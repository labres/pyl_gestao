<?php
  if(isset($_GET['erro'])){
    ?>
    <script src="vendors/jquery/jquery.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>  
    <script>
        jQuery(function($){
          retornoGet();
        });
    </script> 
    <?php
  }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PYL gestão</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="css/style.css" <!-- End layout styles -->
    <link rel="shortcut icon" href="images/logo-pyl.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="text-center brand-logo">
                  <img src="images/logo-pyl-login.png">
                </div>
                <div class="text-center">
                  <h6>Informe o e-mail cadastrado para que possamos enviar um link para criar uma nova senha.<h6>
                </div>
                <div style="display:none" id="mensagemErro" class="text-center">
                  <h4 class="card-title text-danger">E-mail não cadastrado</h4>
                </div>
                <form class="pt-3" action="check-login.php" method="post">
                <div class="form-group">
                    <input type="email" name="emailRecuperacao" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="E-mail" required>
                  </div>
                  <div class="mt-3">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">Enviar</button>
                  </div> 
                </form>
                <a href="index.php"><button class="btn btn-block btn-link btn-lg font-weight-medium auth-form-btn" type="submit">Cancelar</button></a>
              </div>
              <a class="link-desenvolvedor" style="text-decoration: none" href="http://labsoft.tech/" target="_blank" ><p style="text-align:center; width:100%;" class="text-muted">&nbsp Desenvolvido por <span class="text-primary" style="font-size:1.2em"><b>LAB</b>soft</span></p></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <script src="js/off-canvas.js"></script>
    <script src="js/misc.js"></script>
  </body>
</html>

<script>
  function retornoGet(){
    document.getElementById('mensagemErro').style.display = "block";
  }
</script>
<?php
function modal($evento, $retorno){
  
    switch($retorno){
        case true :{ 
            $class = 'success';
            $titulo = "Sucesso! :)";
            $msg = $evento." com êxito.";
            break;
        }
        case false : { 
            $class = 'danger';
            $titulo = "Ops! :(";
            $msg = "Algo deu errado. Por favor tente mais tarde";
            break;
        }
    }

    return alert($class, $titulo, $msg); 
}

function modalEmail($retorno){
  switch($retorno){
      case 'sucesso' :{ 
          $class = 'success';
          $titulo = "Sucesso! :)";
          $msg = "Email enviado com sucesso.";
          break;
      }
      case 'erro' : { 
          $class = 'danger';
          $titulo = "Ops! :(";
          $msg = "Algo deu errado. Por favor tente mais tarde";
          break;
      }
  }
  return alert($class, $titulo, $msg);
}

function alert($class, $titulo, $msg){
  return '
  <div class="alert alert-'.$class.'" role="alert">
      <strong>'.$titulo.'</strong><br>
      '.$msg.'
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>';
}


<?php
include_once 'secao.php';
require_once 'notificacao.php';

ini_set ("display_errors", "On"); 
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
if(isset($_GET['erroAutenticacao'])){
  ?>
    <script>
        jQuery(function($){
          retornoGet();
        });
    </script> 
  <?php
}

date_default_timezone_set("America/Sao_Paulo");

$printAlert = "";
if(isset($_GET['mensagem']) && $_GET['mensagem'] != null){
  $printAlert = modalEmail($_GET['mensagem']);

}

?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PYL gestão</title>
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="css/style.css" <!-- End layout styles -->
    <link rel="shortcut icon" href="images/logo-pyl.png" />
  </head>
  <body>
  
    <div class="container-scroller">
      <?php echo $printAlert ?>
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="text-center brand-logo">
                  <img src="images/logo-pyl-login.png">
                </div>
                <div style="display:none" id="mensagemErro" class="text-center">
                  <h4 class="card-title text-danger">E-mail ou senha não conferem.</h4>
                </div>
                <form class="pt-3" action="check-login.php" method="post">
                  <div class="form-group">
                    <input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="E-mail">
                  </div>
                  <div class="form-group">
                    <input type="password" name="senha" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Senha">
                  </div>
                  <div class="mt-3">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">Entrar</button>
                  </div>
                  <div class="my-2 d-flex justify-content-between align-items-center">
                    <a href="direcionador.php" class="auth-link text-black">Esqueceu a senha?</a>
                  </div>
                  
                </form>
              </div>
              <a class="link-desenvolvedor" style="text-decoration: none" href="http://labsoft.tech/" target="_blank" ><p style="text-align:center; width:100%;" class="text-muted">&nbsp Desenvolvido por <span class="text-primary" style="font-size:1.2em"><b>LAB</b>soft</span></p></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <script src="js/off-canvas.js"></script>
    <script src="js/misc.js"></script>
    <script src="js/funcoes.js"></script>
  </body>
</html>

<script>
  function retornoGet(){
    document.getElementById('mensagemErro').style.display = "block";
  }

</script>
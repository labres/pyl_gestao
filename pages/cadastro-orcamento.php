

<div class="form-group">
    <label for="solcitante">Solicitante *</label>
    <select id="solcitante" name="solicitante" class="form-control has-error" required>
        <option value="">Selecione</option>
        <?php
        foreach($usuarios as $usuario){
            
            echo "<option value=".$usuario->getId().">".$usuario->getNome()."</option>";
        }
    ?>
    </select>
</div>
<div class="form-group">
    <label for="comprador">Comprador *</label>
    <select class="form-control not-required" name="comprador" id="comprador">
        <option value="">Selecione</option>
        <?php
        foreach( $buyers as $buyer ){
            echo '<option value='.$buyer->getId().'>'.$buyer->getNome().'</option>';
        }
        ?>
    </select>
</div>
<div class="form-group">
    <label for="descricao">Descrição</label>
    <textarea type="text" rows="3" name="descriao" class="form-control" id="descricao" placeholder="Descrição" maxlength="1000" ></textarea> 
</div>


<div class="form-group grupo-item" data-id="itemBox0" id="itemBox0">
    <div class="form-group align-itens">
        <div class="form-group input-block">
            <label for="exampleSelectGender">Item *</label>
            <select id="item" name="item[]" class="col form-control" id="exampleSelectGender">
                <option value="">Selecione</option>
                    <?php
                foreach($itemArray as $item){
                    
                    echo '<option value='.$item->getId().'>'.$item->getDescricao().'</option>';
                    
                }
                ?>
            </select>
        </div>
        <div class="form-group componente">
            <a href="index.php?conteudo=lista-itens.php"><span class="btn-body-none"><i class="icon-plus"></i></span></a>
        </div>
    </div>
    <div>
        <div class="box-label">
            <label class="label-iten">Valor (R$)</label>
            <label class="label-iten">Tempo (Hr)</label>
            <label class="label-iten">Total (R$)</label>
        </div><br>
        <div class="box-iten">                                   
            <input class="form-control valor input-iten" type="text" name="valor[]" 
                onkeyup="calcValue(this)" placeholder="Valor" onfocus="moneyFormat()" title="Valor (R$)">

            <input class="form-control input-iten" type="number" name="tempo[]" onkeyup="calcValue(this)"
                placeholder="Tempo" title="Tempo (Hr)" >

            <span class="field-hidden" name="valueHidden"></span>
            <p class="form-control input-iten" name="total[]" title="Total" placeholder="Total"></p>

            <button type="button" class="btn btn-danger not-required"
                onclick="remove(this)">Excluir Item</button>

        </div>
    </div>
</div>
<div class="form-total-iten">
    <input class="btn btn-primary mr-2" id="btn-add-iten" type="button" name="button" value="Adicionar item" />
    <div class="form-group form-label-center">
        <label for="totalQuote">Valor Total</label>
        <input class="form-control" type="text" name="totalQuote" id="totalQuote" value="0"/>
    </div>
</div>
<hr>
<h4 class="card-description">Custos de logística</h4>
<div class="form-group-flex-start">
    <div class="form-group">
        <label for="transporte">Transporte</label>
        <input class="form-control valor not-required" type="text" name="transporte" id="transporte" onfocus="moneyFormat()" onkeyup="calcQuotesAdditional(this, '#alimentacao', '#hospedagem')">
    </div>
    <div class="form-group">
        <label for="alimentacao">Alimentação</label>
        <input class="form-control valor not-required" type="text" name="alimentacao" id="alimentacao" onfocus="moneyFormat()" onkeyup="calcQuotesAdditional(this, '#transporte', '#hospedagem')">
    </div>
    <div class="form-group">
        <label for="hospedagem">Hospedagem</label>
        <input class="form-control valor not-required" type="text" name="hospedagem" id="hospedagem" onfocus="moneyFormat()" onkeyup="calcQuotesAdditional(this, '#transporte', '#alimentacao')">
    </div>
</div>
<hr>
<h4 class="card-description">Informações complementares</h4>
<div class="form-group-flex-start">
    <div class="form-group">
        <label for="prazo">Prazo de entrega (dias)</label>
        <input class="form-control" type="number" name="prazo" id="prazo" >
    </div>
    <div class="form-group">
        <label for="validade">Validade da proposta (dias)</label>
        <input class="form-control" type="number" name="validade" id="validade" >
    </div>
    <div class="form-group">
        <label for="condicao">Condição de pagamento (dias)</label>
        <input class="form-control" type="number" name="condicao" id="condicao">
    </div>
</div>
<hr>
<h4 class="card-description">Resumo da proposta</h4>
<h6><small class="text-muted"> COTAÇÃO: </small><?php echo $codeOrcamento ?></h6><br>
<div class="form-group-flex-start grupo-item">
    <div class="form-group">
        <label for="total-services">Valor total dos serviços</label>
        <p class="text-white resume" id="totalServicos">R$ 0,00</p>
    </div>
    <div class="form-group">
        <label for="total-logistica">Custos de logística</label>
        <p class="text-white resume" id="totalLogistica">R$ 0,00</p>
    </div>
    <div class="form-group">
        <label for="imposto">Impostos</label>
        <input type="hidden" name="aliquota" id="aliquota" value="<?php echo $configuracao->getValue() ?>">
        <p class="text-white resume" id="imposto">R$ 0,00</p>
    </div>
    <div class="form-group">
        <label for="totalQuoteFinal">Valor total da cotação</label>
        <p class="text-white resume font-weight-bold" id="totalQuoteFinal">R$ 0,00</p>
    </div>
    
</div>
<input type="hidden" name="typeSave" id="typeSave">
<button type="submit" class="btn btn-primary mr-2 save not-required" id="draft">Salvar Rascunho</button>
<button type="button" class="btn btn-primary mr-2 save not-required" id="done">Salvar e Enviar</button>
<a href="index.php?conteudo=lista-usuario.php" class="btn btn-light">Cancelar</a>
                       
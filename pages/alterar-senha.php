<?php
  require_once '../controlador/tipoUsuario-controlador.php';
  require_once '../modelo/class-usuario.php';
  require_once '../controlador/cadastroUsuario-controlador.php';
  require_once '../conn.php';
  require_once '../secao.php';

  if(isset($_POST['idUsuario']) && $_POST['idUsuario'] != null){
    if(editarSenha($_POST, $MySQLi) == true){
      echo '<script>window.location = "index.php?conteudo=home.php&senha=sucesso"</script>';
    }
  }
?>

<div class="main-panel">
  <div class="row">
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Alterar senha</h4>
          <p class="card-description"> Mínimo de 6 caracteres e máximo de 10.</p>
          <div style="display:none" id="mensagemErro">
            <h4 id="msgErro" class="card-title">Senhas não conferem.</h4>
          </div>
          <form class="forms-sample" method="post" action="#">
            <input type="hidden" name="idUsuario" class="form-control" value="<?=$_SESSION['id'] ?>">
            <div class="form-group">
              <label for="exampleInputPassword4">Senha</label>
              <input onkeyUp="comparaSenhas()" name="senha1" value="" type="password" class="form-control" id="senha1" placeholder="Password" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword4">Repita a Senha</label>
              <input onkeyUp="comparaSenhas()" name="senha2" value="" type="password" class="form-control" id="senha2" placeholder="Password" required>
            </div>
            <button type="submit" class="btn btn-primary mr-2" id="btn-alterar" disabled="disabled">Alterar Senha</button>
            <button class="btn btn-light">Cancelar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function comparaSenhas(){
    senha1 = document.getElementById('senha1').value;
    senha2 = document.getElementById('senha2').value;
    if(senha2 != ""){
      if(senha1 === senha2){ 
        document.getElementById('msgErro').innerHTML = 'Senhas conferem :)';
        document.getElementById('msgErro').style.color = 'green';
        document.getElementById('btn-alterar').disabled = false;
      }else{
        document.getElementById('mensagemErro').style.display = 'block';
        document.getElementById('msgErro').innerHTML =  'Senhas não conferem :(';
        document.getElementById('msgErro').style.color = 'red';
        document.getElementById('btn-alterar').disabled = true;
      }
    }
  }
</script>
<?php
  require_once '../controlador/tipoUsuario-controlador.php';
  require_once '../modelo/class-cliente.php';
  require_once '../controlador/cliente-controlador.php';
  require_once '../conn.php';

  $acao = $_GET['acao'];
  $textoBotao = 'Salvar';
  
if(isset($_POST['acao']) && $_POST['acao'] != null){
    
    switch($_POST['acao']){
      case 'salvar' : {
        salvarCliente($_POST, $MySQLi);
        break;
      }
      case 'salvarEdicao' : {
        editarCliente($_POST, $MySQLi);
        break;
      }
    }
  }

  $cliente = new Cliente();
  if(isset($_POST['editar']) && $_POST['editar'] != null){
    $cliente = buscarCliente($_POST['editar'], $MySQLi);
    $acao = "salvarEdicao";
    $textoBotao = 'Salvar Alterações';
  }


  $tipoUsuarioArray = array();
  $tipoUsuarioArray = buscarTiposUsuarios($MySQLi);
?>

<div class="main-panel">
  <div class="row">
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Cadastro de Clientes</h4>
          <p class="card-description"> * Obrigatório</p>
          <form class="forms-sample" method="post" action="#">
            <input type="hidden" name="acao" class="form-control" value="<?=$acao ?>">
            <input type="hidden" name="idCliente" class="form-control" value="<?=$cliente->getId() ?>">
            <div class="form-group">
              <label for="exampleInputName1">Nome *</label>
              <input type="text" name="nome" value="<?php if($cliente->getNome() != null) echo $cliente->getNome() ?>" class="form-control" id="exampleInputName1" placeholder="Nome" required>
            </div>
            <div class="form-group">
                <label for="codigo">Codigo</label>
                <input class="form-control" placeholder="Código" type="number" name="codigo" id="codigo" minlength="4">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">cnpj *</label>
              <input type="text" name="cnpj" data-mask="00.000.000/0000-00" value="<?php if($cliente->getCnpj() != null) echo $cliente->getCnpj() ?>" class="form-control" placeholder="CNPJ" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Telefone</label>
              <input type="fone" class="form-control" data-mask="(00)0000-0000" name="telefone" value="<?php if($cliente->getTelefone() != null) echo $cliente->getTelefone() ?>" class="form-control" id="exampleInputEmail3" placeholder="Telefone">
            </div>
            <button type="submit" class="btn btn-primary mr-2"><?=$textoBotao ?></button>
            <a href="index.php?conteudo=lista-cliente.php" class="btn btn-light">Cancelar</a>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</div>

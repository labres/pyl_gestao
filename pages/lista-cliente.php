<?php
    require_once '../controlador/cliente-controlador.php';
    require_once '../notificacao.php';
    require_once '../conn.php';
    require_once '../secao.php';

    $printAlert = "";

    switch($_GET['notificacao']){
        case 'salvo': {$printAlert = modal('seu cadastro foi salvo', true); break;}
        case 'excluido': {$printAlert = modal('Exclusão realizada', true); break;}
        case 'editado' : {$printAlert = modal('seu cadastro foi alterado', true); break;}
        case 'erro' : $printAlert = modal('', false);
    }

    $listaClientes = array();
    $listaClientes = buscarClientes($MySQLi);
      
?>  

  <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <?php echo $printAlert; ?>
                    <h4 class="card-title">Clientes</h4>
                    <div class="card-body">
                        <form method="post" action="index.php?conteudo=cadastro-cliente.php&acao=salvar">    
                            <button type="submit" class="btn btn-primary">Novo Cliente</button>
                        </form>
                    </div>
                    <p class="card-description"> Lista de Clientes</p>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                <?php
                                    if($dispositivoMovel == true){ 
                                        echo '<th>Nome</th>
                                        <th>Excluir</th>';
                                    }else{
                                        echo '<th>Nome</th>
                                        <th>Telefone</th>
                                        <th>CNPJ</th>
                                        <th class="text-center">Editar</th>
                                        <th class="text-center">Excluir</th>';
                                    }  ?>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php
                                    //function for mobile
                                        foreach($listaClientes as $cliente){
                                            if($dispositivoMovel == true){
                                                echo '<tr>
                                                <td>
                                                    <form method="post" action="index.php?conteudo=cadastro-cliente.php">
                                                    <input type="hidden" name="acao" value="editar">
                                                    <input type="hidden" name="editar" value="'.$cliente->getId().'">
                                                    <button type="submit" type="button" class="btn btn-link btn-sm">'.$cliente->getNome().'</button>
                                                </td>';
                                            }else{
                                                echo '<tr><td>'.$cliente->getNome().'</td>';
                                                echo '<td>'.$cliente->getTelefone() .'</td>';
                                                echo '<td>'.$cliente->getCnpj() .'</td>';
                                                ?> 
                                                <td class="text-center">
                                                <form method="post" action="index.php?conteudo=cadastro-cliente.php">
                                                    <input type="hidden" name="acao" value="editar">
                                                    <input type="hidden" name="editar" value="<?php echo $cliente->getId() ?>">
                                                    <button type="submit" type="button" class="btn btn-link btn-sm"><i class="icon-pencil"></i></button>
                                                </form>
                                                </td>
                                                <?php
                                            }

                                        ?>
                                        <td class="text-center">
                                            <input type="hidden" name="excluir" value="<?php echo $cliente->getId() ?>">
                                            <button data-target="#modalConfirm" data-toggle="modal" type="submit" type="button" class="btn btn-link btn-sm" onclick="getCont(<?=$cliente->getId() ?>)"><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                        
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
        <!-- modal -->
        <div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="../controlador/cliente-controlador.php" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Questão</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <input id="idDeleteModal"  type="hidden" name="idExcluir">
                        <div class="modal-body">
                            Tem certeza que quer excluir este cliente?
                        </div>
                        <div class="text-center">
                            <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                            <button type="submit" class="btn btn-primary">Sim</button>  
                        </div><br>
                    </div>
                </form>
            </div>
        </div>
 
        
        <script src="../vendors/jquery/jquery.min.js"></script>
        <script>
            function getCont(id){
                $("#idDeleteModal").val(id);
            }

            $(".alert").delay(4000).slideUp(200, function() {
                $(this).alert('close');
            });

        </script>

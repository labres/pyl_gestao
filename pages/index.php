<?php
require_once '../secao.php';
require_once '../notificacao.php';

if($_SESSION['nome'] == null) header('LOCATION:../index.php');

$nome_abreviado = explode(" ", $_SESSION['nome']);

$conteudo = 'home.php';

if(isset($_GET['conteudo'])) {
    $conteudo = $_GET['conteudo'];

    if($conteudo == 'sair') {
        session_destroy();
        header('Location:../index.php');
    }
}

?>
<style>

.footer-custom{
    background-color: #fff !important; 
    width: 82% !important;
    display: flex !important;
    justify-content: center !important;
    align-items: center !important;
    margin-bottom: 0px !important;
    bottom: 0px !important;
    position: fixed !important;
}
</style>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PYL gestão de serviços</title>
    
    <link rel="stylesheet" href="../vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="../vendors/select2/select2.min.css">
    <link rel="stylesheet" href="../vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css" >
    <link rel="stylesheet" href="../css/estilo.css" >
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="../images/logo-pyl.png" />
  </head>
  <body>
    <div class="container-scroller">
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div style="background-color:#000000" >
         
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center flex-grow-1">
          <a class="logo-index" href="index.php">
            <img src="../images/logo-pyl.png" alt="logo" class="img-logo-index" />
          </a>
          <h5 class="mb-0 font-weight-medium d-none d-lg-flex">Sistema de Gestão de Serviços</h5>
          <ul class="navbar-nav navbar-nav-right ml-auto">
            <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span  class="font-weight-normal"> <?php echo $nome_abreviado[0]." ".$nome_abreviado[1]?> </span></a>
              <div  class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                  <p class="mb-1 mt-3"><?php echo  $nome_abreviado[0]." ".$nome_abreviado[1] ?></p>
                  <p class="font-weight-light text-muted mb-0"><?php echo $_SESSION['email'] ?> </p>
                </div>
                <a class="dropdown-item" href="index.php?conteudo=alterar-senha.php"><i class="dropdown-item-icon icon-pencil text-primary"></i> Alterar senha</a>
                <a class="dropdown-item" href="index.php?conteudo=sair"><i class="dropdown-item-icon icon-power text-primary"></i>Sair</a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="icon-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="text-wrapper">
                  <p class="profile-name"><?php echo $nome_abreviado[0]." ".$nome_abreviado[1] ?></p>
                  <p class="designation"><?php echo $_SESSION['tipo'] ?></p>
                </div>
              </a>
            </li>
            <li class="nav-item nav-category"><span class="nav-link">Funções</span></li>
            <li class="nav-item">
                <a class="nav-link"  href="index.php?conteudo=cadastro-pre-orcamento.php">
                  <span class="menu-title">Orçamentos</span>
                  <i class="icon-doc menu-icon"></i>
                </a>
                <a class="nav-link"  href="../createPDF.php">
                  <span class="menu-title">PDF</span>
                  <i class="icon-doc menu-icon"></i>
                </a>
                <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="index.php?conteudo=lista-itens.php">Itens</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item nav-category"><span class="nav-link">Administrador</span></li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Cadastros</span>
                <i class="icon-layers menu-icon"></i>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="index.php?conteudo=lista-usuario.php">Usuário</a></li>
                  <li class="nav-item"> <a class="nav-link" href="index.php?conteudo=lista-cliente.php">Cliente</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item nav-category"><span class="nav-link">Pessoal</span></li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?conteudo=alterar-senha.php">
                <span class="menu-title">Alterar Senha</span>
                <i class="icon-pencil menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?conteudo=sair">
                <span class="menu-title">Sair</span>
                <i class="icon-power menu-icon"></i>
              </a>
            </li>
          </ul>
        </nav>
        <div class="main-panel">
          <?php
            include($conteudo);
            
          ?>
            <div class="footer-custom">
                <a class="link-desenvolvedor text-center" style="text-decoration: none" href="http://labsoft.tech/"
                    target="_blank">
                    <p style="text-align:center; width:100%;" class="text-muted">&nbsp Desenvolvido por <span
                            class="text-primary" style="font-size:1.2em"><b>LAB</b>soft</span></p>
                </a>
            </div>
        </div>

      </div>
    </div>
    <script src="../js/funcoes.js"></script>
    <script src="../vendors/js/vendor.bundle.base.js"></script>
    <script src="../vendors/select2/select2.min.js"></script>
    <script src="../vendors/typeahead.js/typeahead.bundle.min.js"></script>
    <script src="../js/off-canvas.js"></script>
    <script src="../js/misc.js"></script>
    <script src="../js/typeahead.js"></script>
    <script src="../js/select2.js"></script>
    <script src="../vendors/jquery/jquery.min.js"></script>
    <script src="../vendors/jquery/jquery.maskMoney.js"></script>
    <script src="../vendors/jquery/jquery.mask.min.js"></script>
  </body>
</html>
<?php
require_once '../modelo/class-usuario.php';
require_once '../controlador/item-controlador.php';
require_once '../controlador/cliente-controlador.php';
require_once '../controlador/cargo-controlador.php';
require_once '../controlador/cliente-controlador.php';
require_once '../controlador/cadastroUsuario-controlador.php';
require_once '../controlador/configuracao-controlador.php';
require_once '../controlador/orcamento-controlador.php';
require_once '../conn.php';
require_once '../secao.php';

$bodyQuote = '';

$usuarios = $codeOrcamento = '';
if ( isset( $_GET['content'] ) && $_GET['content'] ){
    $bodyQuote = $_GET['content'];
    $idCompany = $_GET['customer']; 
    $usuarios = findUserByCompany( $MySQLi, $idCompany );
    $buyers = findBuyerByCompany( $MySQLi, $idCompany );
    $customer = buscarCliente( $idCompany, $MySQLi );
    $codeOrcamento = buildCodigoOrcamento( $customer->getCodigo() );
}

if( isset( $_POST['codigo']) && $_POST['codigo'] != '' ) {
    save( $MySQLi, $_POST);
}

  
$clientes = array();
$clientes = buscarClientes( $MySQLi );

$configuracao = findConfigurationById( $MySQLi, 1 );

$cargoArray = array();
$cargoArray = buscarCargo($MySQLi);

$itemArray = array();
$itemArray = buscarItens($MySQLi);

?>

<script src="../js/funcoes.js"></script>
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body" id="card-body">

                <h4 class="card-title">Cadastrar orçamento</h4>
                <p class="card-description"> * Obrigatório</p>
                <form class="forms-sample" method="post" id="form" action="#">
                    <input type="hidden" name="codigo" id="codigo" value="<?=$codeOrcamento ?>">
                    <input type="hidden" name="idUsuario" class="form-control" value="<?=$_SESSION['id'] ?>">
                    <div class="form-group">
                        <label for="empresa">Empresa</label>
                        <select class="form-control" name="empresa" id="empresa" required>
                            <option value="0" selected>Selecione</option>
                            <?php
                            foreach( $clientes as $cliente ){
                                $selected = '';
                                if ( $cliente->getId() == $idCompany ) $selected = 'selected';
                                echo '<option value="'. $cliente->getId() .'" '. $selected .'  >'. $cliente->getNome() .'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <?php
                    include($bodyQuote);
                    ?>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalConfirm" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <form method="post" action="orcamento-controlador.php">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Questão</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" name="save" id="save" />
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Salvar Rascunho</button>
                    <button type="submit" class="btn btn-primary">Salvar e Enviar</button>
                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="../vendors/jquery/jquery.min.js"></script>
<script src="../vendors/jquery/jquery.maskMoney.js"></script>
<script src="../vendors/jquery/jquery.mask.min.js"></script>

<script>

var countIten = 0;
var elements = ['itemBox0'];
let totalQuoteFloat = 0;
let flagVerifyfields = true;

function moneyFormat() {
    $(".valor").maskMoney();
}

function moneyFormat() {
    $(".valor").maskMoney({
        prefix: "R$ ",
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
}

function calcValue(field) {

    let valueField;
    let valueSibling;
    let valueTotal;
    let totalItemfloat = 0;

    valueSibling = $(field).siblings('input');
    let tot = 0;

    if ( field.name == 'valor[]' ) {
        valueField = parseCurrencyToFloat(field.value);
        valueSibling = parseToInt(valueSibling[0].value);
    }else{
        valueField = parseToInt(field.value);
        valueSibling = parseCurrencyToFloat(valueSibling[0].value);
    }

    if ( valueField > 0 && valueSibling > 0){
        tot = floatToCurrency(valueField * valueSibling);
        $(field).parent('div').find('span').html( valueField * valueSibling );
        $(field).parent('div').find('p').html( tot );
    }else{
        $(field).parent('div').find('p').html( floatToCurrency(0) );
    }

    $('span[name="valueHidden"]').each( function(){
        if ( this.innerHTML != '' )
        totalItemfloat = totalItemfloat + parseFloat(this.innerHTML);
    })

    updateTotais( totalItemfloat );
} 

function updateTotais( totalItemfloat ){
    $('#totalQuote').val( floatToCurrency( totalItemfloat ) );
    $('#totalServicos').html( floatToCurrency( totalItemfloat ) );

    updateFinalTotal( totalItemfloat, 'servicos' );
}

function updateFinalTotal( value, idField ){
    let total = 0;
    let field = ''; 
    if ( idField == 'servicos' ){
        field = $('#totalLogistica').html();
        total = value + parseCurrencyToFloat( field );
        calcTaxAndTotais( total );
    } 

    else{
        field = $('#totalServicos').html();
        total = value + parseCurrencyToFloat( field );
        calcTaxAndTotais( total );
    }
    
}

function calcTaxAndTotais( total ){

    //calc percent
    let aliquota = parseToInt( $('#aliquota').val() ) ;
    let tax = total * ( aliquota / 100 );
    $('#imposto').html( floatToCurrency( tax ) );

    //final total
    $('#totalQuoteFinal').html( floatToCurrency( tax + total ) );

}

function remove(component) {

if (elements.length > 1) {
    let target = $(component).closest('[data-id]');
    let totalValue = $(component).parent('div').find('span').html();
    let totalItemFloat = 0;
    if ( totalValue.trim() ) {
        total =  parseCurrencyToFloat( $('#totalQuote').val() ) - parseFloat( totalValue ) ;
        updateTotais( total );
    }

    target.remove();
    elements.splice(elements.indexOf(target.data('id')), 1);
    countIten--;
}

}

function floatToCurrency(value){
    return value.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
}

function parseToInt(value){
    return parseInt(value);
}

function parseCurrencyToFloat(value){
    result = value.replace('&nbsp;', '');
    result = result.replace('.', '');
    result = result.replace('R$', '');
    result = result.replace(' ', '');
    return parseFloat(result.replace(',', '.'));
}

$('#btn-add-iten').click(function() {
    let newIten = $('#' + elements[elements.length - 1]).clone().find("input").val("").end();
    $(newIten).insertAfter("#itemBox" + countIten);
    countIten++;
    newIten.find('p').html('');
    newIten.find('span').html('');
    newIten.attr('id', 'itemBox' + countIten);
    newIten.attr('data-id', 'itemBox' + countIten);
    elements.push('itemBox' + countIten);

});

$('#empresa').change( function() {

    if( $(this).val() != 'Selecione' && $(this).val() != '' ){
        $(location).attr('href', 'index.php?conteudo=cadastro-pre-orcamento.php&content=cadastro-orcamento.php&customer=' + $(this).val());
    }else{
        $(location).attr('href', 'index.php?conteudo=cadastro-pre-orcamento.php');
    }
})

function calcTotalServices(){
    alert('test');
}

function calcQuotesAdditional(element, firstFieldSum, secondFieldSum){

    let total = 0;
    let firstField = $(firstFieldSum);
    let secondField = $(secondFieldSum);
    let arrayFields = [firstField, secondField];

    if ( element.value.trim() ) total = parseCurrencyToFloat( element.value );

    arrayFields.forEach( function( item ){
        if ( item.val().trim() ){
            total += parseCurrencyToFloat( item.val() );
        }
    })

    $('#totalLogistica').html( floatToCurrency(total) );

    updateFinalTotal( total, 'logistica' );
        
}

$('.save').click(function(){
    flagVerifyfields = true;
    let id = $(this).attr('id');
    $('#typeSave').val( id );

    if ( id == 'save' ){

        $("#form :input").each(function(){
            verifyFieldEmpty( $(this) );
        });

        if ( !flagVerifyfields ) { 

            $('#card-body').prepend( prependAlert() );
            $(window).scrollTop(0);
        }
        else $("#form").submit();
     
    }
})

function verifyFieldEmpty( element ){

    if( (element[0].value == '' ||
        element[0].value == undefined ||
        element[0].value == null ||
        element[0].value == '0') && 
        !element.hasClass( "not-required" ) ) {

        element.addClass('is-invalid');
        flagVerifyfields = false;
        return;
    }else{
        element.removeClass('is-invalid');
    }
   
    return;
}

function prependAlert(){
    
    let alertFieldRequired = ' <div class="alert alert-danger" role="alert" id="alert-required">';
    alertFieldRequired += '<strong>Favor informar os camps obrigatórios (x) </strong>';
    alertFieldRequired += ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">';
    alertFieldRequired += '<span aria-hidden="true">&times;</span> </button></div>';
    return alertFieldRequired;
}


</script>
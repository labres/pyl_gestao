<?php
  require_once '../controlador/tipoUsuario-controlador.php';
  require_once '../modelo/class-usuario.php';
  require_once '../controlador/cadastroUsuario-controlador.php';
  require_once '../controlador/cliente-controlador.php';
  require_once '../controlador/cargo-controlador.php';
  require_once '../conn.php';
  require_once '../secao.php';
  $request = md5(implode( $_POST ));

  $acao = $_GET['acao'];
  $textoBotao = 'Salvar';

  if($_SESSION['last_request'] == $request)
    if(isset($_POST['acao']) && $_POST['acao'] != null){
      
      switch($_POST['acao']){
        case 'salvar' : {
          salvarUsuario($_POST, $MySQLi);
          unset($_POST);
          break;
        }
        case 'salvarEdicao' : {
          editarUsuario($_POST, $MySQLi);
          unset($_POST);
          break;
        }
        case 'salvarCargo' : {
          salvarCargo($_POST, $MySQLi);
          unset($_POST);
          break;
        }
      }
    }
  

  $usuario = new Usuario();
  if(isset($_POST['editar']) && $_POST['editar'] != null){
    $usuario = buscarUsuario($_POST['editar'], $MySQLi);
    $acao = "salvarEdicao";
    $textoBotao = 'Salvar Alterações';
  }
  $tipoUsuarioArray = array();
  $tipoUsuarioArray = buscarTiposUsuarios($MySQLi);

  $cargoArray = array();
  $cargoArray = buscarCargo($MySQLi);

  $clienteArray = array();
  $clienteArray = buscarClientes($MySQLi);
?>

<div class="main-panel">
  <div class="row">
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Cadastro de Usuários</h4>
          <p class="card-description"> * Obrigatório</p>
          <form class="forms-sample" method="post" action="#">
            <input type="hidden" name="acao" class="form-control" value="<?=$acao ?>">
            <input type="hidden" name="idUsuario" class="form-control" value="<?=$usuario->getId() ?>">
            <div class="form-group">
              <label for="exampleInputName1">Nome *</label>
              <input type="text" name="nome" value="<?php if($usuario->getNome() != null) echo $usuario->getNome() ?>" class="form-control" id="exampleInputName1" placeholder="Nome" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Email *</label>
              <input type="email" name="email" value="<?php if($usuario->getEmail() != null) echo $usuario->getEmail() ?>" class="form-control" id="exampleInputEmail3" placeholder="E-mail" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Telefone</label>
              <input type="fone" class="form-control" data-mask="(00)0000-0000" name="telefone" value="<?php if($usuario->getTelefone() != null) echo $usuario->getTelefone() ?>" class="form-control" id="exampleInputEmail3" placeholder="Telefone">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail3">Celular</label>
              <input type="fone" class="form-control" data-mask="(00)00000-0000" name="celular" value="<?php if($usuario->getCelular() != null) echo $usuario->getCelular() ?>" class="form-control" id="exampleInputEmail3" placeholder="Celular">
            </div>
            <div class="form-group">
              <label for="exampleSelectGender">Tipo de Usuário *</label>
              <select id="tipoUsuario" name="tipoUsuario" class="form-control" id="exampleSelectGender" onchange="verificaTipo()" required>
                <option value="0">Selecione</option>
              <?php
                foreach($tipoUsuarioArray as $tipoUsuario){
                  if($tipoUsuario->getId() == $usuario->getIdTipo()){
                    echo "<option value='".$tipoUsuario->getId()."' selected='selected'>".$tipoUsuario->getDescricao()."</option>";
                  }else{
                    echo "<option value='".$tipoUsuario->getId()."'>".$tipoUsuario->getDescricao()."</option>";
                  }
                }
              ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleSelectGender">Empresa</label>   
              <select id="cliente" name="cliente" class="form-control" id="exampleSelectGender">
                <option value="null" >Selecione</option>
              <?php
                foreach($clienteArray as $cliente){
                  if($cliente->getId() == $usuario->getIdCliente()){
                    echo "<option value='".$cliente->getId()."' selected='selected'>".$cliente->getNome()."</option>";
                  }else{
                    echo "<option value='".$cliente->getId()."'>".$cliente->getNome()."</option>";
                  }
                }
              ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword4">Código</label>
              <input id="codigoColaborador" name="codigo" value="<?php if($usuario->getCodigo() != null) echo $usuario->getCodigo() ?>" type="text" class="form-control" placeholder="Código">
            </div>
            <div style="display: flex;align-items: flex-end;" class="form-group">
              <div style="width: 100%;">
                <label for="exampleSelectGender">Cargo *</label>
                <select id="cargo" name="cargo" class="form-control" id="exampleSelectGender">
                  <option value="0">Selecione</option>
                <?php
                  foreach($cargoArray as $cargo){
                    if($cargo->getId() == $usuario->getIdCargo()){
                      echo "<option value='".$cargo->getId()."' selected='selected'>".$cargo->getDescricao()."</option>";
                    }else{
                      echo "<option value='".$cargo->getId()."'>".$cargo->getDescricao()."</option>";
                    }
                  }
                ?>
                </select>
              </div>
              <button style="margin-left:10px; font-size: 1.5em;" type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="modal" data-target="#modalForm">
                +
              </button>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword4">Senha *</label>
              <input  value="<?php if($usuario->getSenha() != null) echo $usuario->getSenha() ?>" type="password" class="form-control" id="exampleInputPassword4" placeholder="Password" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword4">Repita a Senha *</label>
              <input name="senha" value="<?php if($usuario->getSenha() != null) echo $usuario->getSenha() ?>" type="password" class="form-control" id="exampleInputPassword4" placeholder="Password" required>
            </div>
            <button type="submit" class="btn btn-primary mr-2"><?=$textoBotao ?></button>
            <a href="index.php?conteudo=lista-usuario.php" class="btn btn-light">Cancelar</a>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="#" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cadastrar Cargo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="hidden" name="acao" value="salvarCargo">
          <div class="modal-body">
          <div class="form-group">
              <label for="exampleInputPassword4">Cargo</label>
              <input  name="cargo" type="text" class="form-control"  placeholder="Cargo" required>
            </div>
          </div>
          <div class="text-center">
          <a href="index.php" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
          <button type="submit" class="btn btn-primary">Salvar</button>  
          </div><br>
          <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Lista de cargos</h4>
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th> Descrição</th>
                          <th class="text-center"> Excluir </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach($cargoArray as $cargo){
                          echo 
                          '<tr>
                              <td class="py-1">
                                '.$cargo->getDescricao().'
                              </td>
                              <td class="text-center">
                                <form method="post" action="#">
                                  <input type="hidden" name="excluirCargo" value="'.$cargo->getId().'">
                                  <button type="submit" type="button" class="btn btn-link btn-sm"><i class="icon-trash"></i></button>
                                </form>
                              </td>
                            </tr>';
                        }
                        ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
        </div>
        
      </form>
      
    </div>
  </div>

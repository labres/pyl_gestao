<?php

require_once '../conn.php';
require_once '../secao.php';
require_once '../notificacao.php';
require_once '../controlador/item-controlador.php';

$request = md5(implode( $_POST ));

$printModal = '';

if($_SESSION['last_request'] == $request){

    if ( isset($_POST['item']) && $_POST['item'] != null ){
        salvarItem( $_POST, $MySQLi );
        $printModal =  modal('seu cadastro foi salvo', true);
    }

    if ( isset( $_POST['idDelete'] ) && $_POST['idDelete'] != null ){
        $test = removeItemById( $MySQLi, $_POST['idDelete'] );
        echo $test;
        if($test) $printModal = modal('Exclusão realizada', true);
        else $printModal = modal('', false);
        
    }
}


$itens = array();
$itens = buscarItens( $MySQLi );

?>

<div class="main-panel">
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <?php echo $printModal ?>
                    <h4 class="card-title" >Cadastro de itens</h4>
                    <p class="card-description">* Obrigatório</p>                  
                    <form class="forms-sample" method="post" action="#">
                        <div class="form-group">
                            <label for="item">Descrição do item</label>
                            <input class="form-control" id="item" type="text" name="item" placeholder="Descrição" required />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Salvar</button>
                        </div>
                    </form> 
                    <br>
                    <hr>
                    <h4 class="card-description">Lista de Itens</h4>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-nover">
                            <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th class="text-center">Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach( $itens as $item ){
                                    echo '<tr>
                                            <td>'
                                                . $item->getDescricao() .
                                            '</td>';
                                    ?>
                                        <td class="text-center"> 
                                            <button data-target="#modalConfirm" data-toggle="modal" class="btn btn-link btn-sm" type="submit" onclick="getCont(<?=$item->getId() ?>)" >
                                                <i class="icon-trash"></i>
                                            </button>        
                                        </td>    
                                    </tr>
                                    <?php        
                                    }
                                ?> 
                            </tbody>
                        </table>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <form  method="post" action="#">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Questão</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" name="idDelete" id="idItemDelete">
                <div class="modal-body">
                    Tem certeza que quer excluir este item?
                </div>
                <div class="text-center">
                    <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                    <button type="submit" class="btn btn-primary" >Sim</button>
                </div><br>
            </div>
        </form>
    </div>
</div>


<script src="../vendor/jquery/jquery.min.js"></script>
<script>    
    function getCont(id){
        $('#idItemDelete').val(id);
    }

    $(".alert").delay(4000).slideUp(200, function() {
        $(this).alert('close');
    });
</script>
<?php
require_once '../notificacao.php';

$printAlert = ''; 

if(isset($_GET['senha']) && $_GET['senha'] != null){
    
    $printAlert = modal('Senha alterada', true); 
    
}
      
?>  
<div class="main-panel">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <?php echo $printAlert; ?>
                    <h4 class="card-title">Home</h4>         
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../vendors/jquery/jquery.min.js"></script>
<script>
$(".alert").delay(4000).slideUp(200, function() {
    $(this).alert('close');
});
</script>
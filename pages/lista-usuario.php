<?php
    require_once '../controlador/cadastroUsuario-controlador.php';
    require_once '../notificacao.php';
    require_once '../conn.php';
    require_once '../secao.php';

    $printModal = "";

    switch($_GET['notificacao']){
        case 'salvo': {$printModal = modal('Seu cadastro foi salvo', true); break;}
        case 'excluido': {$printModal = modal('Exclusão realizada', true); break;}
        case 'editado' : {$printModal = modal('Seu cadastro foi alterado', true); break;}
        case 'erro' : $printModal = modal('', false);
    }

    $listasuarios = array();
    $listasuarios = buscarUsuarios($MySQLi);
      
    ?> 
       
<div class="main-panel">
  <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <?php echo $printModal ?>
                    <h4 class="card-title">Usuários</h4>
                    <div class="card-body">
                        <div>    
                            <A href="index.php?conteudo=cadastro-usuario.php&acao=salvar" ><button class="btn btn-primary">Novo Usuário</button></a>
                        </div>
                    </div>
                    <p class="card-description"> Lista de Usuários</p>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <?php
                                    if($dispositivoMovel == false){ echo '<th>E-mail</th>
                                    <th>Tipo de Usuário</th>';}  ?>
                                    <th class="text-center">Editar</th>
                                    <th class="text-center">Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php
                                    //loop for version mobile
                                    $cont = 0;
                                    foreach($listasuarios as $usuario){
                                        
                                        $disabled = '';
                                        if($usuario->getDescricao()== 'Administrador') $disabled='disabled';
                                        if($dispositivoMovel == true){
                                            $nomeAbreviado = explode(" ", $usuario->getNome());
                                            echo '<tr><td>'.$nomeAbreviado[0]." ".$nomeAbreviado[1];
                                        }else{
                                            echo '<td>'.$usuario->getNome() .'</td>';
                                            echo '<td>'.$usuario->getEmail() .'</td>';
                                            echo '<td>'.$usuario->getDescricao() .'</td>';
                                        }

                                        ?>
                                        <td class="text-center">
                                            <form method="post" action="index.php?conteudo=cadastro-usuario.php">
                                                <input type="hidden" name="acao" value="editar">
                                                <input type="hidden" name="editar" value="<?php echo $usuario->getId() ?>">
                                                <button type="submit"  class="btn btn-link btn-sm" <?php echo $disabled ?>><i class="icon-pencil" ></i></button>
                                            </form>
                                        </td>
                                        <td class="text-center">
                                            <button data-target="#modalConfirm" data-toggle="modal" id="delete" type="submit" class="btn btn-link btn-sm" onclick="getCont(<?=$usuario->getId() ?>)" <?php echo $disabled ?>><i class="icon-trash"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                            $cont++;
                                        }
                                    ?>
                                        
                                </tr> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="../controlador/cadastroUsuario-controlador.php" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Questão</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <input id="idDeleteModal"  type="hidden" name="idExcluir">
                        <div class="modal-body">
                            Tem certeza que quer excluir este usuário?
                        </div>
                        <div class="text-center">
                            <a href="#" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                            <button type="submit" class="btn btn-primary">Sim</button>  
                        </div><br>
                    </div>
                </form>
            </div>
        </div>
    
    
    <script src="../vendors/jquery/jquery.min.js"></script>
    <script>
        function getCont(id){
            $("#idDeleteModal").val(id);
        }

        $(".alert").delay(4000).slideUp(200, function() {
            $(this).alert('close');
        });

    </script>
        

 

 
